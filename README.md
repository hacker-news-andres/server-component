# articles server-component

## Description
This is a backend server that connects to the HackerNews API and gets recent articles from Node.js

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

> You need setup a mongo database and paste uri connection string in the .env file, by replacing credentials of course.

# Usage 
Once you have configured mongo connection, you need to request /articles/populate in order to get records from the hacker news api saved into local database.

Then, you can request following endpoints available on the rest api:

| Verb	| Endpoint	|  	|  	|  	|
|-	|-	|-	|-	|-	|
| GET	| /articles	| Get all articles registered in local database	|  	|  	|
| GET	| /articles/populate	| Extract articles from Hacker News API and save into local database	|  	|  	|
| POST	| /articles/create	| Add an individual article	| {}	|  	|
| DELETE	| /articles/:id	| Remove an article by specifying an id	|  	|  	|

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

