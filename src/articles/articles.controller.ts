import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Res,
} from '@nestjs/common';
import { ArticleDTO } from './dto/ArticleDto';
import { ArticlesService } from './articles.service';

@Controller('articles')
export class ArticlesController {
  constructor(private postsService: ArticlesService) {}

  @Post('/create')
  async create(@Body() params: ArticleDTO, @Res() response: any) {
    try {
      const postCreated = await this.postsService.store(params);
      response.status(HttpStatus.CREATED).json({
        data: postCreated,
      });
    } catch (error) {
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  @Get('/populate')
  async populate(@Res() response: any) {
    const apiItems = this.postsService.getAllFromExternal();
    apiItems.subscribe((res) => {
      if (res.status === 200) {
        const data = res.data['hits'];
        let newArticle: any = '';
        data.forEach(async (value) => {
          newArticle = {
            title: value.story_title ?? value.title,
            url: value.story_url ?? value.url,
            author: value.author,
            points: value.points,
            story_text: value.story_text,
            comment_text: value.comment_text,
            num_comments: value.num_comments,
            tags: value._tags,
            pubdate: value.created_at,
          };
          await this.postsService.store(newArticle);
        });
        response.status(HttpStatus.CREATED).json({
          message: 'Database has been populated.',
        });
      } else {
        response.status(HttpStatus.NO_CONTENT).json({
          message:
            'Database has not been populated. Maybe there is no any data to fetch',
        });
      }
    });
  }

  @Get('/')
  async getAll(@Res() response: any) {
    try {
      const posts = await this.postsService.findAll();
      response.status(HttpStatus.OK).json({
        data: posts,
      });
    } catch (error) {
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  @Delete(':id')
  async remove(@Param() params, @Res() response: any) {
    try {
      const article = await this.postsService.delete(params.id);
      if (article) {
        response.status(HttpStatus.OK).json({
          data: article,
          message: 'Article removed successfully.',
        });
      } else {
        response.status(HttpStatus.NOT_FOUND).json({
          message: 'Article not found or it was already removed before.',
        });
      }
    } catch (error) {
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }
}
