import { Document } from 'mongoose';

export class ArticleDTO extends Document {
  readonly title: string;
  readonly url: string;
  readonly author: string;
  readonly points: number;
  readonly story_text: string;
  readonly comment_text: string;
  readonly num_comments: number;
  readonly tags: Array<string>;
  readonly pubdate: string;
}
