import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
import { Article } from '../schemas/articles.schema';
import { ArticleDTO } from './dto/ArticleDto';
import { IArticles } from './interfaces/articles.interface';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel(Article.name) private readonly articles: Model<ArticleDTO>,
    private httpService: HttpService,
  ) {}

  async store(createPostsDto: ArticleDTO): Promise<IArticles> {
    const createdPosts = await new this.articles(createPostsDto);
    return createdPosts.save();
  }

  async findAll(): Promise<Article[]> {
    return this.articles.find().exec();
  }

  async delete(id: string): Promise<Article> {
    return this.articles.findByIdAndDelete(id).exec();
  }

  getAllFromExternal(): Observable<AxiosResponse<any[]>> {
    return this.httpService.get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
  }
}
