export interface IArticles {
  title: string;
  url: string;
  author: string;
  points: number;
  story_text: string;
  comment_text: string;
  num_comments: number;
  tags: Array<string>;
  pubdate: string;
}
